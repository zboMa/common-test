const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const externalConfig = [
  { name: `vue`, external: `Vue`, url: `https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js` },
  { name: `element-ui`, external: `ELEMENT`, url: `https://cdn.bootcdn.net/ajax/libs/element-ui/2.15.2/index.js` },
  { name: `common`, external: [`MyLib`, `common`], url: `${process.env.VUE_APP_COMMON_URL}/dist/MyLib.common.0.0.1.js` },
  { name: `utils`, external: [`MyLib`, `utils`], url: `${process.env.VUE_APP_COMMON_URL}/dist/MyLib.utils.0.0.1.js` },
]

const externals = {}
externalConfig.map(_item => {
  externals[_item.name] = _item.external
})

module.exports = {
  configureWebpack: {
    externals,
    plugins: [
      new HtmlWebpackPlugin({
        filename: `public/index.html`,
        cdns: externalConfig
      })
    ],
  },
  chainWebpack: config => {
    // 设置项目目录别名，其余使用jsconfig的@/
    config.resolve.alias.set(`@`, path.resolve(`src`)).
      set(`@assets`, path.resolve(`src/assets`)).
      set(`@components`, path.resolve(`src/components`)).
      set(`@views`, path.resolve(`src/views`)).
      set(`@static`, path.resolve(`src/static`))
  },
  devServer: {
    port: 8089
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            'primary-color': `#147290`, // 全局主色
            'primary-color-secondary': `#147290d8`, // 全局主色
            'primary-white': `rgba(255, 255, 255, 0.85)`, // 全局主色
            'primary-white-secondary': `rgba(255, 255, 255, 0.65)`, // 全局主色
            'primary-white-disabled': `rgba(255, 255, 255, 0.45)`, // 全局主色
            'primary-drak': `rgba(0, 0, 0, 0.85)`, // 全局主色
            'primary-drak-secondary': `rgba(0, 0, 0, 0.65)`, // 全局主色
            'primary-drak-disabled': `rgba(0, 0, 0, 0.45)`, // 全局主色

            'link-color': `#147290`, // 链接色
            'success-color': `#009966`, // 成功色
            'sign-color': `#913D10`, // 标记色
            'warning-color': `#faad14`, // 警告色
            'error-color': `#D82431`, // 错误色

            'heading-color': `rgba(0, 0, 0, 0.85)`, // 标题色
            'text-color': `rgba(0, 0, 0, 0.85)`, // 主文本色
            'text-color-secondary': `rgba(0, 0, 0, .65)`, // 次文本色
            'disabled-color': `rgba(0, 0, 0, .45)`, // 失效色

            'dividing-line-color': `#eeeeee`, // 分割线颜色

            'border-color-base': `#a0a0a0`, // 边框色
            'border-color-base-secondary': `#cccccc`, // 边框色
            'border-base': `1px solid @border-color-base`,
            'border-base-secondary': `1px solid @border-color-base-secondary`,

            'border-radius-base': `10px`,
            'border-radius-sm': `6px`,

            'font-size-huge': `18px`, // 一级标题字体大小
            'font-size-lg': `16px`, // 二级标题字体大小
            'font-size-base': `14px`, // 正文字体大小
            'font-size-sm': `12px`, // 次文本字体大小

            'slider-gutter-huge': `20px`,
            'slider-gutter-lg': `16px`,
            'slider-gutter': `12px`,
            'slider-gutter-sm': `8px`,
            'slider-gutter-xs': `4px`,
          },
          javascriptEnabled: true,
        },
      },
    },
    sourceMap: true,
  }
}